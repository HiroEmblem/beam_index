//
//  ShareUserViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-12-02.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

class ShareUserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
 
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    private var users: [Fulluser] = []
    public var listId = 0
    public var userUsername = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        MarthaRequest.fetchUserList(PK_FK_list_id: listId,completion: { (users) in
            if let fetchedUser = users {
                self.users = fetchedUser
            }
        })
        
        refreshData()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self

    }
    
    public func refreshData(){
        
        MarthaRequest.fetchUserList(PK_FK_list_id: listId,completion: { (users) in
            if let fetchedUser = users {
                self.users = fetchedUser
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)-> Int {
        return users.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "user-cell", for: indexPath) as! Usercell
        cell.textLabel?.text = users[indexPath.row].firstname + users[indexPath.row].lastname
        cell.detailTextLabel?.text = users[indexPath.row].username
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete"){
            (action, view, completion) in
            MarthaRequest.deleteUserList(PK_FK_user_id: self.users[indexPath.row].pk_id, PK_FK_list_id: self.listId )
            DispatchQueue.main.async {
                self.users.remove(at: indexPath.row)
                self.tableView.reloadData()
                let alertController = UIAlertController(title: "User removed!", message:
                    "User has been removed", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                self.present(alertController, animated: true, completion: nil)
 
            }
            completion(true)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    
    @IBAction func onAddButtonTapped(_ sender: Any) {
        if let username = usernameTextField.text {
            if username != userUsername {
            
            MarthaRequest.fetchedUser(username: username,completion: { (users) in
                    if  let user = users {
                    var alreadyIn = false
                        if user.count > 0 {
                    for u in self.users {
                        if u.username == user[0].username{
                             alreadyIn = true
                            print(u.username)
                            print(user[0].username)
                        }
                    }
                    if alreadyIn == false{
                    MarthaRequest.addUserList(PK_FK_user_id: user[0].pk_id, PK_FK_list_id: self.listId, completion: {(Finished) in
                    if Finished!
                        {
                            DispatchQueue.main.async
                            {
                                self.refreshData()
                                let alertController = UIAlertController(title: "Information saved!", message:
                                "User added", preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                                }))
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    })
                    }
                    else
                    {
                    DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Information not saved", message:"User is already in the list", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    }))
                    self.present(alertController, animated: true, completion: nil)
                    }
                  }
                }
                    else{
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "Information not saved", message:"Username does not exit", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                            }))
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
            })
          }
    else{
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: "Information not saved", message:"Cannot add yourself", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
        }))
        self.present(alertController, animated: true, completion: nil)
        }
    }
    }
    }
    
}

