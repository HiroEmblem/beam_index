//
//  MyGroupsViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-25.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit
class MyGroupsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource
{
    public var users: [Fulluser] = []
    private var groups: [Group] = []
    var itemId = 0
    public var usernameUser = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshData()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    
    @IBAction func onRefeshButtonTapped(_ sender: Any) {
         refreshData()
    }

    public func refreshData(){
        
        MarthaRequest.fetchGroup(username: users[0].username,completion: { (groups) in
            if let fetchedGroups = groups {
                self.groups = fetchedGroups
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)-> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "list-cell", for: indexPath) as! Listcell
        cell.label?.text = groups[indexPath.row].groupname
        cell.detailTextLabel?.text = groups[indexPath.row].description
        print(groups[0].groupname)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        itemId = groups[indexPath.row].PK_id
        self.performSegue(withIdentifier: "modifyGroupSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /*if segue.destination is ModifyListViewController
        {
            let vc = segue.destination as? ModifyGroupViewController
            vc?.modifyListId = itemId
        }*/
        if segue.destination is AddGroupViewController
        {
            let vc = segue.destination as? AddGroupViewController
            vc?.idUser = self.users[0].pk_id
        }
        if segue.destination is ModifyGroupViewController
        {
            let vc = segue.destination as? ModifyGroupViewController
            vc?.modifyGroupId = itemId
            vc?.users = self.users
            
        }
    }
    
        func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: "Delete"){
                (action, view, completion) in
                MarthaRequest.deleteGroup(id: self.groups[indexPath.row].PK_id)
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Group Deleted!", message: "Group has been deleted", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                        
                        //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                    }))
                    self.present(alert, animated: true)
                
                    self.groups.remove(at: indexPath.row)
                    self.tableView.reloadData()
                }
                completion(true)
            }
            return UISwipeActionsConfiguration(actions: [deleteAction])
        }
        
        
    @IBAction func onAddButtonTapped(_ sender: Any) {
    }
    

        func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
            return .none
        }
    
    
    @IBAction func unwindFromAddGroup(segue: UIStoryboardSegue){
        self.refreshData()
    }
    
}

