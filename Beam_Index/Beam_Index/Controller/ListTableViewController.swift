//
//  ListTableViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

class ListTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private var lists: [List] = []
    private var sharedList: [List] = []
    
    var itemId = 0
   @IBOutlet weak var tableView: UITableView!
    public var usernameUser = ""
    public var users: [Fulluser] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        MarthaRequest.fetchedUser(username: usernameUser,completion: { (users) in
            if let fetchedUser = users {
                self.users = fetchedUser
                DispatchQueue .main.async {
                    self.refreshData()
                }
            }
        })

        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    @IBAction func GroupsTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "MyGroupSegue", sender: nil)
    }
    
    @IBAction func MyAccountTapped(_ sender: Any) {
       self.performSegue(withIdentifier: "MyAccountSegue", sender: nil)
    }
    
    @IBAction func onRefreshButtonTaped(_ sender: Any) {
        refreshData()
    }
    public func refreshData(){
        MarthaRequest.fetchList(username: usernameUser,completion: { (lists) in
            if let fetchedLists = lists {
                self.lists = fetchedLists
                MarthaRequest.fetchUserListLists(PK_FK_user_id: self.users[0].pk_id,completion: { (lists) in
                    if let fetchedList = lists {
                        self.sharedList = fetchedList
                        DispatchQueue .main.async {
                            self.tableView.reloadData()
                        }
                    }
                })
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)-> Int {
        if section == 0
        {
            if lists.count > 0
            {
               return lists.count
            }
            else if sharedList.count > 0
            {
                return sharedList.count
            }
        }else if section == 1
        {
            return sharedList.count
        }

         return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "list-cell", for: indexPath) as! Listcell
        if indexPath.section == 0
        {
            if lists.count > 0
            {
                cell.label?.text = lists[indexPath.row].name
                cell.detailTextLabel?.text = lists[indexPath.row].description
            }
            else if sharedList.count > 0
            {
                 cell.label?.text = sharedList[indexPath.row].name
                 cell.detailTextLabel?.text = sharedList[indexPath.row].description
            }
        }else if indexPath.section == 1
        {
                cell.label?.text = sharedList[indexPath.row].name
                cell.detailTextLabel?.text = sharedList[indexPath.row].description
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.section == 0
        {
            if lists.count > 0
            {
                itemId = lists[indexPath.row].PK_id
                self.performSegue(withIdentifier: "listModifySegue", sender: self)
            }
            else if sharedList.count > 0
            {
                
            }
        }else if indexPath.section == 1
        {
        }
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ModifyListViewController
        {
            let vc = segue.destination as? ModifyListViewController
            vc?.modifyListId = itemId
            vc?.userUsername = users[0].username
            
        }
        if segue.destination is AddListViewController
        {
            let vc = segue.destination as? AddListViewController
            vc?.idUser = self.users[0].pk_id
        }
        if segue.destination is MyAccountViewController
        {
            let vc = segue.destination as? MyAccountViewController
            vc?.user = self.users
            
            //vc?.user.append(self.users[0])
        }
        if segue.destination is MyGroupsViewController
        {
            let vc = segue.destination as? MyGroupsViewController
            vc?.users.append(self.users[0])
        }
    }
   
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section == 0
        {
            if lists.count > 0
            {
                let deleteAction = UIContextualAction(style: .destructive, title: "Delete"){
                    (action, view, completion) in
                    MarthaRequest.deleteList(id: self.lists[indexPath.row].PK_id)
                    DispatchQueue.main.async {
                        self.lists.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        let alertController = UIAlertController(title: "List deleted!", message:
                            "List has been deleted", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    completion(true)
                }
                return UISwipeActionsConfiguration(actions: [deleteAction])

            }
            else if sharedList.count > 0
            {
                let deleteAction = UIContextualAction(style: .destructive, title: "Remove from my shared list"){
                    (action, view, completion) in
                    MarthaRequest.deleteUserList(PK_FK_user_id: self.users[0].pk_id, PK_FK_list_id: self.sharedList[indexPath.row].PK_id)
                    DispatchQueue.main.async {
                        self.sharedList.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        let alertController = UIAlertController(title: "List removed!", message:
                            "This shared list has been removed", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    completion(true)
                }
                return UISwipeActionsConfiguration(actions: [deleteAction])
            }
        }else if indexPath.section == 1
        {
            let deleteAction = UIContextualAction(style: .destructive, title: "Remove from my shared list"){
                (action, view, completion) in
               MarthaRequest.deleteUserList(PK_FK_user_id: self.users[0].pk_id, PK_FK_list_id: self.sharedList[indexPath.row].PK_id)
                DispatchQueue.main.async {
                    self.sharedList.remove(at: indexPath.row)
                    self.tableView.reloadData()
                    let alertController = UIAlertController(title: "List removed!", message:
                        "This shared list has been removed", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                completion(true)
            }
            return UISwipeActionsConfiguration(actions: [deleteAction])
        }
        

        return nil
    }
    
    
    
    @IBAction func onAddButtonTaped(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    @IBAction func unwindFromCreateList(segue: UIStoryboardSegue){
        self.refreshData()
    }
    
    @IBAction func unwindFromModifyList(segue: UIStoryboardSegue){
        self.refreshData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if lists.count >  0 && sharedList.count >  0 {
            return 2;
        }
        return 1;
    }
   
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if lists.count >  0 && sharedList.count >  0 {
            if section == 0{
                if lists.count > 0{
                    return "My lists"
                }else if sharedList.count > 0{
                    return "My friends lists"
                }
            }
            else if section == 1{
                return "My friends lists"
            }
        } else if section == 0{
            if lists.count > 0{
                return "My lists"
            }else if sharedList.count > 0{
                return "My friends lists"
            }
        }
        return "No lists"
    }
}
