//
//  ConnectionViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-18.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

class ConnectionViewController : UIViewController{
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var passWord: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = nil
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        MarthaRequest.UserExist(username: userName.text!, password: passWord.text!) { (Exist) in
            if(Exist==true)
            {//loggon successfull
                DispatchQueue.main.async {
                    
                        //self.navigationItem.backBarButtonItem = back
                        self.performSegue(withIdentifier: "LoginSegue", sender: nil)

                    
                    //self.present(alert, animated: true)
                    
                    defaults.set(self.userName.text!, forKey: "username")
                    defaults.set(self.passWord.text!, forKey: "password")
                    self.userName.text!=""
                    self.passWord.text!=""
                }
            }
            else
            {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: "Invalid Username or Password", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ListTableViewController
        {
            let vc = segue.destination as? ListTableViewController
            vc?.usernameUser = userName.text!
        }
    }
}

