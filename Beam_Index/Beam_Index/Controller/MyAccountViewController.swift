//
//  MyAccountViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-22.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit
class MyAccountViewController : UIViewController{
    
    public var user: [Fulluser] = []
    public var user2: [Fulluser] = []
    public var pk_id = 0
    public var usernam=" "
    
    @IBOutlet weak var currentPasswordTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet weak var lastnameTextfield: UITextField!
    @IBOutlet weak var firstnameTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MarthaRequest.fetchUserWithid(PK_id: user[0].pk_id,completion: { (users) in
            if let fetchedUser = users {
                self.user = fetchedUser
            }
        })
        usernam = user[0].username
        pk_id = user[0].pk_id
        firstnameTextfield.text!=self.user[0].firstname
        lastnameTextfield.text!=self.user[0].lastname
        
    }
    public func refreshData(){
        MarthaRequest.fetchUserWithid(PK_id: pk_id,completion: { (users) in
            if let fetchedUser = users {
                self.user = fetchedUser
            }
        })

    }
    
    @IBAction func DeleteButton(_ sender: Any) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Warning", message: "You are about to delete your account. Are you sure?", preferredStyle: .alert)
        
            alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {_ in
                MarthaRequest.deleteAccount(id: self.pk_id)
                self.performSegue(withIdentifier: "ToMainPage", sender: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {_ in
                
                //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
            }))
            
            self.present(alert, animated: true)
        }
        
    }
    @IBAction func FirstnameSave(_ sender: Any) {
        refreshData()
        
        MarthaRequest.updateFirstname(id: pk_id,fn: firstnameTextfield.text!)
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: "Firstname Changed to "+self.firstnameTextfield.text!, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {_ in
            
            //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
        }))
            self.present(alert, animated: true)
        }
    }
    @IBAction func LastnameSave(_ sender: Any) {
        refreshData()
        MarthaRequest.updateLastname(id: pk_id,ln: lastnameTextfield.text!)
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: "Lastname Changed to "+self.lastnameTextfield.text!, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {_ in
            
            //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
        }))
            self.present(alert, animated: true)
            
        }
    }
    @IBAction func PasswordSave(_ sender: Any) {
        if(passwordTextfield.text==confirmPasswordTextfield.text)
        {
            MarthaRequest.UserExist(username: usernam, password: currentPasswordTextfield.text!) { (Exist) in
                if(Exist==true)
                {//loggon successfull
                    MarthaRequest.updatePassword(id: self.pk_id,pw: self.passwordTextfield.text!)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Success", message: "Password changed!", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Woohoo!", style: .default, handler: {_ in
                            self.currentPasswordTextfield.text=""
                            self.passwordTextfield.text!=""
                            self.confirmPasswordTextfield.text!=""
                            //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                        }))
                        self.present(alert, animated: true)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Error", message: "Wrong Password", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Dismiss", style: .default))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
}
