//
//  AddGroupViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-25.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

class AddGroupViewController : UIViewController{
    

    @IBOutlet weak var groupnameTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextView!
   private var groups: [Group] = []
    public var idUser = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        descriptionTextField.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        descriptionTextField.layer.borderWidth = 1.0
        descriptionTextField.layer.cornerRadius = 5
        descriptionTextField.text = "Description"
        descriptionTextField.textColor = UIColor.black
    }
    	
    @IBAction func onDoneButtonTapped(_ sender: Any) {
        let trimmed = groupnameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if(trimmed != "")
        {
            let groupname = groupnameTextField.text!
            let description = descriptionTextField.text!
            MarthaRequest.addGroup(groupname: groupname, description: description,FK_owner_id: idUser,completion: {(newGroup) in
                if let group = newGroup {
                    self.groups.append(group)
                }
                DispatchQueue.main.async {
                    MarthaRequest.addUserGroup(FK_user_id: self.idUser,FK_team_id: self.groups[0].PK_id)
                    let alertController = UIAlertController(title: "Information saved!", message:
                        "Group Added", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                        
                        self.performSegue(withIdentifier: "GoBack", sender: nil)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            })
        }
        else
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Void error", message: "GroupName cannot be void!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                }))
                self.present(alert, animated: true)
            }
            
        }

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoBack" {
            if let destination = segue.destination as? MyGroupsViewController{
                
            }
            
        }
    }
}
