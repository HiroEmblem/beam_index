//
//  ModifyListViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-18.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

class ModifyGroupViewController : UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    public var users: [Fulluser] = []
    private var group: [Group] = []
    private var items: [Item] = []
    public var modifyGroupId = 0
    public var itemId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        MarthaRequest.fetchOneGroup(id: modifyGroupId, completion: { (group) in
            if let fetchedGroups = group {
                self.group = fetchedGroups
                DispatchQueue.main.async {
                    print(self.group.count)
                    self.nameTextField.text = self.group[0].groupname
                    self.descriptionTextView.text = self.group[0].description
                    self.refreshData()
                }
            }
            
        })
        self.tableView.dataSource = self
        self.tableView.delegate = self
        descriptionTextView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        descriptionTextView.layer.borderWidth = 1.0
        descriptionTextView.layer.cornerRadius = 5
        
        refreshData()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    
    
    private func refreshData(){
        MarthaRequest.fetchUserTeam(FK_team_id: modifyGroupId, completion: { (users) in
            if let fetchedUser = users {
                self.users = fetchedUser
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }

        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)-> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "user-cell", for: indexPath) as! Usercell
        cell.textLabel?.text = users[indexPath.row].firstname + users[indexPath.row].lastname
        cell.detailTextLabel?.text = users[indexPath.row].username
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }    
    @IBAction func onRefreshButtonTaped(_ sender: Any) {
        refreshData()
    }

    @IBAction func onSaveButtonTapped(_ sender: Any) {
        let trimmed = nameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if(trimmed != "")
        {
            let name = nameTextField.text!
            let description = descriptionTextView.text!
            MarthaRequest.updateGroup(id: modifyGroupId,name: name, description: description,completion: {(newGroup) in
                if let group = newGroup {
                    self.group.append(group)
                }
                
            })
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "Modify saved!", message:
                    "Group change saved", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    self.performSegue(withIdentifier: "go_back_group", sender: nil)
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        }
        else
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Void error", message: "GroupName cannot be void!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                }))
                self.present(alert, animated: true)
            }
            
        }

    }
    @IBAction func onAddButtonTapped(_ sender: Any) {
        if let username = username.text {
            
            MarthaRequest.fetchedUser(username: username,completion: { (users) in
                if  let user = users {
                    var alreadyIn = false
                    if user.count > 0 {
                        for u in self.users {
                            if u.username == user[0].username{
                                alreadyIn = true
                                print(u.username)
                                print(user[0].username)
                            }
                        }
                        if alreadyIn == false{
                            MarthaRequest.addUserToGroup(FK_user_id: user[0].pk_id, FK_team_id: self.modifyGroupId, completion: {(Finished) in
                                if Finished!
                                {
                                    DispatchQueue.main.async
                                        {
                                            self.refreshData()
                                            let alertController = UIAlertController(title: "Information saved!", message:
                                                "User added", preferredStyle: .alert)
                                            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                                            }))
                                            self.present(alertController, animated: true, completion: nil)
                                    }
                                }
                            })
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                let alertController = UIAlertController(title: "Information not saved", message:"User is already in the list", preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                                }))
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "Information not saved", message:"Username does not exit", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                            }))
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                }
            })
    }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete"){
            (action, view, completion) in
            MarthaRequest.deleteUserTeam(FK_user_id: self.users[indexPath.row].pk_id, FK_team_id: self.modifyGroupId )
            DispatchQueue.main.async {
                self.users.remove(at: indexPath.row)
                self.tableView.reloadData()
                let alertController = UIAlertController(title: "User removed!", message:
                    "User has been removed", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                self.present(alertController, animated: true, completion: nil)
                
            }
            completion(true)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    @IBAction func unwindFromCreateItem(segue: UIStoryboardSegue){
        self.refreshData()
    }
    
    @IBAction func unwindFromModifyItem(segue: UIStoryboardSegue){
        self.refreshData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go_back_group" {
            if let destination = segue.destination as? MyGroupsViewController{
                let vc = segue.destination as? MyGroupsViewController
                vc?.users = self.users
            }
            
        }
    }
    
}

