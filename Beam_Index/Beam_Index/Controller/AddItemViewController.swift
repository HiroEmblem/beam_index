//
//  AddItemViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-19.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit


class AddItemViewController : UIViewController{
    
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var nameTextField: UITextField!
    private var items: [Item] = []
    public var listId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        descriptionTextView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        descriptionTextView.layer.borderWidth = 1.0
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.text = "Description"
        descriptionTextView.textColor = UIColor.black
    }
    
    @IBAction func onDoneButtonTaped(_ sender: Any) {
        let trimmed = nameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if(trimmed != "")
        {

                let name = nameTextField.text!
                let description = descriptionTextView.text!
                MarthaRequest.addItem(FK_list_id: listId,name: name, description: description,completion: {(newItem) in
                    if let item = newItem {
                        self.items.append(item)
                        
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "Information saved!", message:
                                "Item added", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                                self.performSegue(withIdentifier: "go_back", sender: nil)
                            }))
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                })
        }
        else
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Void error", message: "Item name cannot be void!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                }))
                self.present(alert, animated: true)
            }
            
        }

        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go_back" {
            
        }

    }
    
}
