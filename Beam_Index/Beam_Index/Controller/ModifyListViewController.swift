//
//  ModifyListViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-18.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

class ModifyListViewController : UIViewController, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var tableView: UITableView!
    

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    private var list: [List] = []
    private var items: [Item] = []
    public var modifyListId = 0
    public var itemId: Int = 0
    public var userUsername = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        MarthaRequest.fetchOneList(id: modifyListId, completion: { (list) in
            if let fetchedLists = list {
                self.list = fetchedLists
                DispatchQueue.main.async {
                    print(self.list.count)
                    self.nameTextField.text = self.list[0].name
                    self.descriptionTextView.text = self.list[0].description
                    self.refreshData() 
                }
            }
            
        })
        self.tableView.dataSource = self
        self.tableView.delegate = self
        descriptionTextView.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        descriptionTextView.layer.borderWidth = 1.0
        descriptionTextView.layer.cornerRadius = 5
        
    }

    
    private func refreshData(){
        MarthaRequest.fetchItems(id: modifyListId, completion: { (items) in
            if let fetchedItems = items {
                self.items = fetchedItems
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        itemId = items[indexPath.row].PK_id
        self.performSegue(withIdentifier: "itemModifySegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ModifyItemViewController
        {
            let vc = segue.destination as? ModifyItemViewController
            vc?.itemId = self.itemId
            
        }
        if segue.destination is ShareUserViewController
        {
            let vc = segue.destination as? ShareUserViewController
            vc?.listId = self.list[0].PK_id
            vc?.userUsername = self.userUsername
        }
        if segue.destination is AddItemViewController
        {
            let vc = segue.destination as? AddItemViewController
            vc?.listId = self.list[0].PK_id
        }
        if segue.identifier == "go_back" {
            if let destination = segue.destination as? ListTableViewController{
                
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item-cell", for: indexPath) as! Itemcell
        cell.label?.text = items[indexPath.row].name
        cell.detailTextLabel?.text = items[indexPath.row].description
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    @IBAction func onRefreshButtonTaped(_ sender: Any) {
        refreshData()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete"){
            (action, view, completion) in
            MarthaRequest.deleteItem(id: self.items[indexPath.row].PK_id)
            DispatchQueue.main.async {
                self.items.remove(at: indexPath.row)
                self.tableView.reloadData()
                let alertController = UIAlertController(title: "Item deleted!", message:
                    "Item has been removed", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
            completion(true)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    @IBAction func onSavedButtonTaped(_ sender: Any) {
        let trimmed = nameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if(trimmed != "")
        {
            let name = nameTextField.text!
            let description = descriptionTextView.text!
            MarthaRequest.updateList(id: modifyListId,name: name, description: description,completion: {(newList) in
                if let list = newList {
                    self.list.append(list)
                }

            })
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "Modify saved!", message:
                    "List change saved", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    self.performSegue(withIdentifier: "go_back", sender: nil)
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        }
        else
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Void error", message: "List name cannot be void!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                }))
                self.present(alert, animated: true)
            }
            
        }

    }
    
    @IBAction func onDoneButtonTaped(_ sender: Any) {
        
    }
    
    @IBAction func unwindFromCreateItem(segue: UIStoryboardSegue){
        self.refreshData()
    }
    
    @IBAction func unwindFromModifyItem(segue: UIStoryboardSegue){
        self.refreshData()
    }

}
