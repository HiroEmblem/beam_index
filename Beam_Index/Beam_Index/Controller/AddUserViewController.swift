//
//  AddListViewController.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-08.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

class AddUserViewController : UIViewController{
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var passWord: UITextField!
    @IBOutlet weak var passWordConfirm: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerClicked(_ sender: Any) {
        let trimmed = userName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if(trimmed != "")
        {
            if(passWord.text==passWordConfirm.text)
            {

                MarthaRequest.UsernameAvailable(username: userName.text!) { (Available) in
                    if(Available==true)
                    {
                        
                        MarthaRequest.addUser(username: self.userName.text!, password: self.passWord.text!, firstname: self.firstName.text!, lastname: self.lastName.text!, completion: {
                            
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "Success!", message: "User created successfully", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                                    self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                                }))
                                self.present(alert, animated: true)
                                
                                defaults.set(self.userName.text!, forKey: "username")
                                defaults.set(self.passWord.text!, forKey: "password")
                                defaults.set(self.firstName.text!, forKey: "firstname")
                                defaults.set(self.lastName.text!, forKey: "lastname")
                                
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Woops", message: "Username taken!", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                                //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                            }))
                            self.present(alert, animated: true)
                        }
                        
                    }
                }
            }
                else
                {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Woops!", message:
                        "Password mismatch", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        else
        {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Username error", message: "Username cannot be void!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {_ in
                    //self.performSegue(withIdentifier: "RegisterSegue", sender: nil)
                }))
                self.present(alert, animated: true)
            }
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ListTableViewController
        {
            let vc = segue.destination as? ListTableViewController
            vc?.usernameUser = userName.text!
        }
    }
    
}


