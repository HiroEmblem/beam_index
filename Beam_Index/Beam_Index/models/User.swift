import Foundation

struct User {
    var username: String
    var password : String
    var firstname: String
    var lastname: String
    
    init?(json:[String:Any]){
        
        if let username = json["username"] as? String,
            let password = json["password"] as? String,
            let firstname = json["firstname"] as? String,
            let lastname = json["lastname"] as? String{
            self.username = username
            self.password = password
            self.firstname = firstname
            self.lastname = lastname
        }
        else{
            return nil
        }
    }
    
    init(username : String, password : String, firstname: String, lastname: String) {
        self.username = username
        self.password = password
        self.firstname = firstname
        self.lastname = lastname
    }

}
