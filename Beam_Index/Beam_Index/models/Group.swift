//
//  Group.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-25.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import Foundation

struct Group {
    var PK_id: Int
    var groupname: String
    var description : String
    var FK_owner_id: Int
    
    init?(json:[String:Any]){
        
        if let PK_id = json["PK_id"] as? Int,
            let groupname = json["groupname"] as? String,
            let description = json["description"] as? String,
            let FK_owner_id = json["FK_owner_id"] as? Int{
            self.PK_id = PK_id
            self.description = description
            self.groupname = groupname
            self.FK_owner_id = FK_owner_id
            
        }
        else{
            return nil
        }
    }
    
    init(id : Int, groupname : String, description : String){
        self.PK_id = id
        self.groupname = groupname
        self.description = description
        self.FK_owner_id = 2
    }
}
