//
//  Item.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-19.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import Foundation

struct Item{
    var PK_id: Int
    var name: String
    var description : String
    var FK_list_id: Int
    
    init?(json:[String:Any]){
        
        if let PK_id = json["PK_id"] as? Int,
            let name = json["name"] as? String,
            let description = json["description"] as? String,
            let FK_list_id = json["FK_list_id"] as? Int{
            self.PK_id = PK_id
            self.description = description
            self.name = name
            self.FK_list_id = FK_list_id
            
        }
        else{
            return nil
        }
    }
    init(id : Int, name : String, description : String,FK_list_id: Int) {
        self.PK_id = id
        self.name = name
        self.description = description
        self.FK_list_id = FK_list_id
    }
    
    init(id : Int, name : String, description : String) {
        self.PK_id = id
        self.name = name
        self.description = description
        self.FK_list_id = 2
    }
}
