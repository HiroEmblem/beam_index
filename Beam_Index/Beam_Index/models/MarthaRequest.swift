//
//  MarthaRequest.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-04.
//  Copyright © 2019 devoir.go. All rights reserved.
//
import Foundation
 class MarthaRequest
 {

    private static let authTeam = Data("team3:Qc2dqZG6r9".utf8).base64EncodedString()

    
    private static func request(query: String, params: Data? = nil, completion: @escaping ([String:Any]?)-> Void)
    {
        let url = URL(string: "http://martha.jh.shawinigan.info/queries/\(query)/execute")!
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue(MarthaRequest.authTeam, forHTTPHeaderField: "auth")
        request.httpBody = params
        
        let task = session.dataTask(with: request as URLRequest) { (httpData, httpResponse, httpError) in
            if let error = httpError
            {
                print(error)
                completion(nil)
            } else if let data = httpData
            {
                do
                {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    if let json = jsonResponse as? [String:Any]
                    {
                        completion(json)
                    }else
                    {
                        print("Invalid Json")
                        completion(nil)
                    }
                }catch let parsingError
                {
                    print("Parsing error: \(parsingError)")
                    completion(nil)
                }
            } else
            {
                print("Unknown error")
                completion(nil)
            }
            //print("\(httpData)")
            //print("\(httpResponse)")
            //print("\(httpError)")
            
        }
        task.resume()
    }
    
    
    public static func fetchList(username: String,completion: @escaping ([List]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["username": username])
        request(query: "Select-List-With-Username",params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var lists: [List] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let list = List(json: itemJsonObject) {
                            lists.append(list)
                        }
                        
                    }
                    completion(lists)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    public static func fetchGroup(username: String,completion: @escaping ([Group]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["username": username])
        request(query: "Select-Group-With-User",params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var groups: [Group] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let group = Group(json: itemJsonObject) {
                            groups.append(group)
                        }
                        
                    }
                    completion(groups)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    
    public static func UserExist(username: String, password: String, completion: @escaping (Bool?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["username": username, "password": password])
        request(query: "UserExists", params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success && itemsJson.count==1){
                    if let itemJsonObject = itemsJson[0] as? [String:Any],
                        let Exist = itemJsonObject["Exist"] as? Bool{
                        if Exist==true
                        {
                            print("Exists")
                            completion(true)
                        }
                        else
                        {
                            print("Doesnt Exists")
                            completion(false)
                        }
                        
                    }
                }
            }
        }
    }
    
    public static func UsernameAvailable(username: String, completion: @escaping (Bool?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["username": username])
        request(query: "UsernameAvailable", params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success && itemsJson.count==1){
                    if let itemJsonObject = itemsJson[0] as? [String:Any],
                        let Exist = itemJsonObject["Exist"] as? Bool{
                        if Exist==true
                        {
                            print("Exists")
                            completion(true)
                        }
                        else
                        {
                            print("Doesnt Exists")
                            completion(false)
                        }
                        
                    }
                }
            }
        }
    }

    
    public static func addList(FK_id: Int,name : String, description : String, completion: @escaping (List?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["name": name, "description": description,"FK_user_id": FK_id])
        request(query: "Insert-List", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let id = jsonObject["lastInsertId"] as? Int{
                
                if(success){
                    let list = List(id: id,name: name,description: description)
                    completion(list)
                }
            }else{
                print("Add failed")
                completion(nil)
            }
        }
    }
    
    public static func addGroup(groupname : String, description : String, FK_owner_id: Int, completion: @escaping (Group?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["groupname": groupname, "description": description,"FK_owner_id": FK_owner_id])
        request(query: "Insert-Group", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let id = jsonObject["lastInsertId"] as? Int{
                
                if(success){
                    let group = Group(id: id, groupname: groupname,description: description)
                    completion(group)
                }
            }else{
                print("Add failed")
                completion(nil)
            }
        }
    }
    
    public static func addUserToGroup(FK_user_id: Int, FK_team_id: Int, completion: @escaping (Bool?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["FK_user_id": FK_user_id, "FK_team_id": FK_team_id])
        request(query: "Insert-UserTeam", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool{
                if(success){
                    completion(true)
                }
            }else{
                completion(false)
                print("Add failed")
            }
        }
    }
    
    public static func addUserList(PK_FK_user_id: Int,PK_FK_list_id: Int, completion: @escaping (Bool?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_FK_user_id": PK_FK_user_id, "PK_FK_list_id": PK_FK_list_id])
        request(query: "Insert-UserList", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool{
                if(success){
                    completion(true)
                }
            }else{
                completion(false)
                print("Add failed")
            }
        }
    }
    
    public static func addUserGroup(FK_user_id: Int,FK_team_id: Int)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["FK_user_id": FK_user_id, "FK_team_id": FK_team_id])
        request(query: "Insert-UserTeam", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool{
                if(success){
                }
            }else{
                print("Add failed")
            }
        }
    }

    
    public static func addUser(username : String,password : String, firstname : String, lastname : String, completion: @escaping ()-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["username": username, "password": password, "firstname":firstname , "lastname":lastname ])
        request(query: "Insert-User", params: params) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool{
            
                if(success){
                    completion()
                }
            }else{
                print("Fetch failed")
            }
        }
    }



    public static func fetchOneList(id : Int ,completion: @escaping ([List]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id])
        request(query: "Select-List-With-Id", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var lists: [List] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let list = List(json: itemJsonObject) {
                            lists.append(list)
                        }
                    }
                    completion(lists)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    
    public static func fetchOneGroup(id : Int ,completion: @escaping ([Group]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id])
        request(query: "Select-Group-With-Id", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var groups: [Group] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let group = Group(json: itemJsonObject) {
                            groups.append(group)
                        }
                    }
                    completion(groups)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    
    public static func updateList(id : Int,name : String, description : String, completion: @escaping (List?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["name": name, "description": description,"PK_id": id])
        request(query: "Update-List", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let id = jsonObject["lastInsertId"] as? Int{
                
                if(success){
                    let list = List(id: id,name: name,description: description)
                    completion(list)
                }
                else{
                    print("Modify failed")
                    completion(nil)
                }
            }
        }
    }
    
    public static func updateGroup(id : Int,name : String, description : String, completion: @escaping (Group?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["groupname": name, "description": description,"PK_id": id])
        request(query: "Update-Group", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let id = jsonObject["lastInsertId"] as? Int{
                
                if(success){
                    let group = Group(id: id,groupname: name,description: description)
                    completion(group)
                }
                else{
                    print("Modify failed")
                    completion(nil)
                }
            }
        }
    }
    
    public static func deleteList(id : Int)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id])
        request(query: "Delete-List", params: params ) { (jsonObjectResponse) in
        }
    }
    public static func deleteGroup(id : Int)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id])
        request(query: "Delete-Group", params: params ) { (jsonObjectResponse) in
        }
    }
    
    public static func fetchItems(id : Int,completion: @escaping ([Item]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["FK_list_id": id])
        request(query: "Select-Items", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var items: [Item] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let item = Item(json: itemJsonObject) {
                            items.append(item)
                        }
                        
                    }
                    completion(items)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    
    public static func addItem(FK_list_id: Int,name : String, description : String, completion: @escaping (Item?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["FK_list_id": FK_list_id,"name": name, "description": description])
        request(query: "Insert-Item", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let id = jsonObject["lastInsertId"] as? Int{
                
                if(success){
                    let item = Item(id: id,name: name,description: description,FK_list_id: FK_list_id)
                    completion(item)
                }
            }else{
                print("Add failed")
                completion(nil)
            }
        }
    }
    
    public static func fetchedUser(username: String, completion: @escaping ([Fulluser]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["username": username])
        request(query: "Select-Userid-With-Username", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                if(success){
                    var users: [Fulluser] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let user = Fulluser(json: itemJsonObject) {
                            users.append(user)
                        }
                    }
                    completion(users)
                }else{
                    print("Fetch failed")
                }
            } else {
                print("Request failed invalid format")
            }
        }
    }
    
    public static func fetchUserWithid(PK_id: Int, completion: @escaping ([Fulluser]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": PK_id])
        request(query: "Select-User-With-id", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                if(success){
                    var users: [Fulluser] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let user = Fulluser(json: itemJsonObject) {
                            users.append(user)
                            
                        }
                    }
                    completion(users)
                }else{
                    print("Fetch failed")
                }
            } else {
                print("Request failed invalid format")
            }
        }
    }
    
    public static func deleteItem(id : Int)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id])
        request(query: "Delete-Item", params: params ) { (jsonObjectResponse) in
        }
    }
    
    public static func updateItem(id : Int,name : String, description : String, completion: @escaping (Item?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["name": name, "description": description,"PK_id": id])
        request(query: "Update-Item", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let id = jsonObject["lastInsertId"] as? Int{
                
                if(success){
                    let item = Item(id: id,name: name,description: description)
                    completion(item)
                }
                else{
                    print("Modify failed")
                    completion(nil)
                }
            }
        }
    }
    
    public static func fetchOneItem(id : Int ,completion: @escaping ([Item]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id])
        request(query: "Select-Item-With-Id", params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var items: [Item] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let item = Item(json: itemJsonObject) {
                            items.append(item)
                        }
                    }
                    completion(items)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    
    public static func deleteAccount(id : Int)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id])
        request(query: "Delete-User", params: params ) { (jsonObjectResponse) in
        }
    }
    public static func updatePassword(id : Int, pw: String)
    {

        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id,	"password":pw])
        request(query: "Update-Password", params: params ) { (jsonObjectResponse) in
        }
    }
    public static func updateFirstname(id : Int, fn: String)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id, "firstname":fn])
        request(query: "Update-Firstname", params: params ) { (jsonObjectResponse) in
        }
    }
    public static func updateLastname(id : Int, ln: String)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_id": id, "lastname":ln])
        request(query: "Update-Lastname", params: params ) { (jsonObjectResponse) in
        }
    }
    
    public static func fetchUserList(PK_FK_list_id: Int,completion: @escaping ([Fulluser]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_FK_list_id": PK_FK_list_id])
        request(query: "Select-User-With-PK_FK_list_id",params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var users: [Fulluser] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let user = Fulluser(json: itemJsonObject) {
                            users.append(user)
                        }
                        
                    }
                    completion(users)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    public static func deleteUserList(PK_FK_user_id : Int, PK_FK_list_id : Int)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_FK_user_id": PK_FK_user_id,"PK_FK_list_id": PK_FK_list_id ])
        request(query: "Delete-UserList", params: params ) { (jsonObjectResponse) in
        }
    }
    
    public static func deleteUserTeam(FK_user_id : Int, FK_team_id : Int)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["FK_user_id": FK_user_id,"FK_team_id": FK_team_id ])
        request(query: "Delete-UserTeam", params: params ) { (jsonObjectResponse) in
        }
    }
    
    
    public static func fetchUserTeam(FK_team_id: Int,completion: @escaping ([Fulluser]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["FK_team_id": FK_team_id])
        request(query: "Select-User-in-Group",params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                
                if(success){
                    var users: [Fulluser] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let user = Fulluser(json: itemJsonObject) {
                            users.append(user)
                        }
                        
                    }
                    completion(users)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
    
    public static func fetchUserListLists(PK_FK_user_id: Int,completion: @escaping ([List]?)-> Void)
    {
        let params = try! JSONSerialization.data(withJSONObject: ["PK_FK_user_id": PK_FK_user_id])
        request(query: "Select-List-With-Userid",params: params ) { (jsonObjectResponse) in
            if let jsonObject = jsonObjectResponse,
                let success = jsonObject["success"] as? Bool,
                let itemsJson = jsonObject["data"] as? [Any]{
                if(success){
                    var lists: [List] = []
                    for itemJson in itemsJson {
                        if let itemJsonObject = itemJson as? [String:Any],
                            let list = List(json: itemJsonObject) {
                            lists.append(list)
                        }
                        
                    }
                    completion(lists)
                }else{
                    print("Fetch failed")
                    completion(nil)
                }
            } else {
                print("Request failed invalid format")
                completion(nil)
            }
        }
    }
}
