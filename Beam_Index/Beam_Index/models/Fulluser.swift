//
//  Fulluser.swift
//  Beam_Index
//
//  Created by dae2 on 2019-11-20.
//  Copyright © 2019 devoir.go. All rights reserved.
//

import UIKit

struct Fulluser{
    var pk_id: Int
    var username: String
    var firstname: String
    var lastname: String
    
    init?(json:[String:Any]){
        
        if let  pk_id = json["PK_id"] as? Int,
            let username = json["username"] as? String,
            let firstname = json["firstname"] as? String,
            let lastname = json["lastname"] as? String{
            self.pk_id = pk_id
            self.username = username
            self.firstname = firstname
            self.lastname = lastname
        }
        else{
            return nil
        }
    }
}

